This is a simple library to generate test CDA documents using data from a CSV file.

Instructions for building and running from source
-------------------------------------------------

```
git clone git@bitbucket.org:ahatherly/cda-test-generator.git
mvn deploy
java -jar target/cda-test-generator-0.0.1-SNAPSHOT-executable.jar --csv <PATH_TO_CSV> --out <OUTPUT_DIRECTORY>
```

Using the pre-built binary
--------------------------

Go to the [https://bitbucket.org/ahatherly/cda-test-generator/downloads](downloads page)
Download the latest JAR file
Run as follows:

```
java -jar cda-test-generator-0.0.1-SNAPSHOT-executable.jar --csv <PATH_TO_CSV> --out <OUTPUT_DIRECTORY>
```

