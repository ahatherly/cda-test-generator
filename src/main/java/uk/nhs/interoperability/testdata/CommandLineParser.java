package uk.nhs.interoperability.testdata;

import java.io.File;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

public class CommandLineParser {
	private static String CSVPARAM = "csv";
	private static String OUTPUTPARAM = "out";
	
	private String csv = null;
	private File output = null;
	
	public CommandLineParser(String args[]) {
		// --csv=/home/adam/TD002706-ReleaseNoteV2.csv
		// --out=/home/adam/cda/
		OptionParser parser = new OptionParser();
        parser.accepts( CSVPARAM ).withRequiredArg();
        parser.accepts( OUTPUTPARAM ).withRequiredArg();
        //parser.allowsUnrecognizedOptions();
        OptionSet options = parser.parse( args );
        
        if (options.has( CSVPARAM )) {
        	csv = options.valueOf(CSVPARAM).toString();
        } else {
        	System.out.println("You must specify a path to the CSV file with test data in it");
        	System.exit(1);
        }
        if (options.has( OUTPUTPARAM )) {
        	output = new File(options.valueOf(OUTPUTPARAM).toString());
        } else {
        	System.out.println("You must specify the output path to write CDA documents to");
        	System.exit(1);
        }
	}

	public String getCsv() {
		return csv;
	}

	public File getOutput() {
		return output;
	}
}
