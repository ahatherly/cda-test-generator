package uk.nhs.interoperability.testdata;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.exceptions.MissingMandatoryFieldException;
import uk.nhs.interoperability.payloads.helpers.TransferOfCareFields;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;

public class TestDataGenerator {

	public static void main(String[] args) throws IOException, MissingMandatoryFieldException {
		
		// Get options from command line parameters
		CommandLineParser commandline = new CommandLineParser(args);
		File outputPath = commandline.getOutput();
		
		// Create a default set of fields for our document
		final TransferOfCareFields defaultFields = TOCDocumentCreator.createDefaultFields();
		
		Reader in = new FileReader(commandline.getCsv());
		Iterable<CSVRecord> records = CSVFormat.DEFAULT.withHeader().parse(in);
		int count = 0;
		for (CSVRecord record : records) {
		    count++;
			System.out.println(record.get("UNIQUE_ID"));
		    
			// Over-write relevant fields from each row of CSV data
			TransferOfCareFields fields = defaultFields;
			fields.setPatientNHSNo(record.get("NHS_NUMBER"));
			fields.setPatientBirthDate(new DateValue(record.get("DATE_OF_BIRTH")));
			
			PersonName patient = fields.getPatientName(); 
			patient.setFamilyName(record.get("FAMILY_NAME"));
			patient.setGivenName(new ArrayList());
			patient.addGivenName(record.get("GIVEN_NAME"));
			if (hasValue(record.get("OTHER_GIVEN_NAME"))) {
				patient.addGivenName(record.get("OTHER_GIVEN_NAME"));
			}
			patient.setTitle(record.get("TITLE"));
			fields.setPatientName(patient);
			
			if (hasValue(record.get("GENDER"))) {
				// Note: The CSV data included "Indeterminate" which is not in the CDA vocab for Sex.
				if (record.get("GENDER").equals("Male")) fields.setPatientGender(Sex._Male);
				else if (record.get("GENDER").equals("Female")) fields.setPatientGender(Sex._Female);
				else fields.setPatientGender(Sex._Notknown);
			}
			
			Address address = fields.getPatientAddress();
			address.setAddressLine(new ArrayList()); // Start with empty list of lines
			if (hasValue(record.get("ADDRESS_LINE_1"))) address.addAddressLine(record.get("ADDRESS_LINE_1"));
			if (hasValue(record.get("ADDRESS_LINE_2"))) address.addAddressLine(record.get("ADDRESS_LINE_2"));
			if (hasValue(record.get("ADDRESS_LINE_3"))) address.addAddressLine(record.get("ADDRESS_LINE_3"));
			if (hasValue(record.get("ADDRESS_LINE_4"))) address.addAddressLine(record.get("ADDRESS_LINE_4"));
			if (hasValue(record.get("ADDRESS_LINE_5"))) address.addAddressLine(record.get("ADDRESS_LINE_5"));
			if (hasValue(record.get("POST_CODE"))) address.setPostcode(record.get("POST_CODE"));
			fields.setPatientAddress(address);
			
			String cda = TOCDocumentCreator.generateCDA(fields);
			
			String outputFilename = outputPath.getAbsolutePath()
					+ "/TOC-Example-" + record.get("UNIQUE_ID") + "-" + record.get("NHS_NUMBER") + ".xml";
			FileWriter.writeFile(outputFilename, cda.getBytes());
		}
		System.out.println("Tests data generated ("+count+" records)");
	}

	private static boolean hasValue(String str) {
		return (str != null && !str.isEmpty());
	}
}


